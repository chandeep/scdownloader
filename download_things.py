import json
import subprocess
import unicodedata
import re
import urllib
import traceback
import mutagen
from mutagen.id3 import ID3, APIC, error
import soundcloud
import os
import ConfigParser
import sys

# Misc. constants
CONFIG_FILE = "config.ini"
DL_DATA_FILENAME = "dl_data.json"
SC_PAGE_LIMIT = 50

# from the Django lib
# https://github.com/django/django/blob/master/django/utils/text.py
def slugify(value):
    """
    Converts to ASCII. Converts spaces to hyphens. Removes characters that
    aren't alphanumerics, underscores, or hyphens. Converts to lowercase.
    Also strips leading and trailing whitespace.
    """
    value = convert_to_ascii(value)
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    value = (re.sub('[-\s]+', '-', value))
    return value

def convert_to_ascii(value):
    return unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')

# use https://github.com/radubogdan/node-offliberty 
# to get offliberty link
def get_download_link(sc_link):
    shell = True if sys.platform == "win32" else False
    p = subprocess.Popen(['off', sc_link], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=shell)
    out, err = p.communicate();

    # get link from output
    link = out.split()[1].strip()

    # simple check for url
    result = re.match(r'http.*', link)

    if result:
        return link
    else:
        return None

def load_json_data(json_path):
    try:
        with open(json_path) as f:
            d = json.load(f)
            return d
    except:
        print "Failed to load JSON data from:", json_path
        return None

def save_json_data(data, json_path):
    try:
        with open(json_path, "w") as f:
            json.dump( data, f, sort_keys=True, indent=4, separators=(',', ':') )
            print "Saved JSON data to " + json_path
    except:
            print "Failed to save JSON data to:", json_path

def get_500x500_link(url):
    return url.replace("-large", "-t500x500") if url else None

if __name__ == '__main__':
    # read values from config.ini
    config = ConfigParser.ConfigParser()
    config.read(CONFIG_FILE)
    DOWNLOADS_DIR = config.get("dirs", "DOWNLOADS_DIR")
    IMAGES_DIR = config.get("dirs", "IMAGES_DIR")
    SC_CLIENT_ID = config.get("sc", "SC_CLIENT_ID")

    # read command line args for username and page limit
    DOWNLOAD_STUFF = 0
    if len(sys.argv) < 3:
        print "Usage: python download_things.py <username> <download_stuff> [num_songs]"
        print "        - download_stuff: 1 to actually download, 0 to just display favourites"
        print "        - num_songs (optional): number of songs to grab from favourites"
        sys.exit()
    if len(sys.argv) >= 3:
        sc_username = sys.argv[1]
        DOWNLOAD_STUFF = int(sys.argv[2])
        print sys.argv[2], DOWNLOAD_STUFF
    if len(sys.argv) >= 4:
        SC_PAGE_LIMIT = int(sys.argv[3])

    # create download and images directory
    if not os.path.exists(DOWNLOADS_DIR):
        os.makedirs(DOWNLOADS_DIR)
    if not os.path.exists(IMAGES_DIR):
        os.makedirs(IMAGES_DIR)

    # open previous download data json
    saved_data = load_json_data(DL_DATA_FILENAME)
    if saved_data is None:
        print "Could not load previous download data, continuing..."
        saved_data = {}

    # Get favs with soundcloud API
    client = soundcloud.Client(client_id=SC_CLIENT_ID)

    # resolve user id from username
    resolve_url = "https://soundcloud.com/" + sc_username
    sc_user = client.get('/resolve', url=resolve_url)

    # print user info
    print "Info for user:", sc_username
    print "======================================="
    for key in sc_user.keys():
        print key, ":", sc_user.__getattr__(key)

    # prompt to continue
    print
    print "Press ENTER to continue"
    raw_input()

    # grabs <SC_PAGE_LIMIT> number of favs
    get_url = 'users/' + str(sc_user.id) + '/favorites'
    favs = client.get(get_url, limit=SC_PAGE_LIMIT)

    favs.reverse()

    try:
        # iterate through data
        for f in favs:

            # add the favs data to dictionary so we can
            # save it later
            d = {}
            d["artist"] = convert_to_ascii(f.user["username"])
            d["title"] = convert_to_ascii(f.title)
            d["image"] = get_500x500_link(f.artwork_url)
            d["link"] = f.permalink_url

            # print retrieved data
            print "Artist:", d["artist"]
            print "Title:", d["title"]
            print "Image:", d["image"]
            print "Song link:", d["link"]

            if DOWNLOAD_STUFF:
                # get safe filename from artist and title
                d["filename"] = slugify(d["artist"]) + "-" + slugify(d["title"])
                print "filename:", d["filename"]

                mp3_path = DOWNLOADS_DIR + "/" + d["filename"] + ".mp3"
                img_path = IMAGES_DIR + "/"+ d["filename"] + ".jpg"

                print "mp3_path:", mp3_path
                print "img_path:", img_path

                # skip if mp3 already downloaded
                if saved_data.has_key(d["link"]):
                    print "---> File already exists, skipping..."
                    print "-----------------------------"
                    continue

                # get download mp3 link
                dl_link = get_download_link(d["link"])
                if dl_link is None:
                    print "Could not get download link, skipping..."
                    print "-----------------------------"
                    continue

                d["dl_link"] = dl_link
                print "DL link:", d["dl_link"]

                # download mp3 from link
                urllib.urlretrieve(d["dl_link"], mp3_path)

                # download album art
                if d["image"]:
                    urllib.urlretrieve(d["image"], img_path)

                # Edit Artist/Title ID3 tags
                # http://stackoverflow.com/questions/18369188/python-add-id3-tags-to-mp3-file-that-has-no-tags
                print "Adding artist/title ID3 tags..."
                meta = mutagen.File(mp3_path, easy=True)
                meta["title"] = d["title"]
                meta["artist"] = d["artist"]
                meta.save()

                # Embed album art
                if d["image"]:
                    print "Adding album art..."
                    audio = mutagen.mp3.MP3(mp3_path, ID3=ID3)
                    audio.tags.add(
                        APIC(
                            encoding=3, # 3 is for utf-8
                            mime='image/jpeg', # image/jpeg or image/png
                            type=3, # 3 is for the cover image
                            desc=u'Cover',
                            data=open(img_path).read()
                        )
                    )
                    audio.save()

                # add filename + data to saved_data hash
                saved_data[d["link"]] = d

            print "-----------------------------"
    except:
        print "@@@ ERROR @@@"
        print traceback.format_exc()
    finally:
        # pickle the saved_data
        if DOWNLOAD_STUFF: save_json_data(saved_data, DL_DATA_FILENAME)
        print "Done."
